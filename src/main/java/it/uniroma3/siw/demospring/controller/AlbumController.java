package it.uniroma3.siw.demospring.controller;

import it.uniroma3.siw.demospring.model.Album;
import it.uniroma3.siw.demospring.model.Foto;
import it.uniroma3.siw.demospring.services.AlbumService;
import it.uniroma3.siw.demospring.services.FotoService;
import it.uniroma3.siw.demospring.services.FotografoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@AllArgsConstructor
public class AlbumController {

    private FotografoService fotografoService;
    private AlbumService albumService;
    private FotoService fotografiaService;

    @RequestMapping(value = "/album/tuttiByFotografo/{id}", method = RequestMethod.GET)
    public String visualizzaAlbumFotografo(@PathVariable("id") Long id, Model model) {
        List<Album> tuttiAlbum = this.albumService.albumPerFotografo(this.fotografoService.fotografoPerId(id));
        model.addAttribute("listaAlbum", tuttiAlbum);
        model.addAttribute("fotografo", this.fotografoService.fotografoPerId(id));
        return "tuttiGliAlbumByFotografo.html";
    }

    @RequestMapping(value = "/admin/album/tuttiByFotografo/{id}", method = RequestMethod.GET)
    public String visualizzaEModificaAlbum(@PathVariable("id") Long idFotografo, Model model){
        List<Album> tuttiAlbum = this.albumService.albumPerFotografo(this.fotografoService.fotografoPerId(idFotografo));
        model.addAttribute("listaAlbum", tuttiAlbum);
        model.addAttribute("fotografo", this.fotografoService.fotografoPerId(idFotografo));
        return "tuttiGliAlbumByFotografoADMIN.html";
    }

    @RequestMapping(value = "/admin/album/aggiungiUnAlbum/{id}", method = RequestMethod.GET)
    public String aggiungiUnAlbum(@PathVariable("id") Long id, Model model){
       // model.addAttribute("fotografo", this.fotografoService.fotografoPerId(id));
        model.addAttribute("album", new Album());
        model.addAttribute("idFotografo", id);
        return "albumForm.html";
    }

    @RequestMapping(value="/album/visualizza/{id}", method = RequestMethod.GET)
    public String visualizzaAlbum(@PathVariable("id") Long idAlbum, Model model){
        List<Foto> fotografie = this.fotografiaService.findByAlbum(this.albumService.findById(idAlbum));
        model.addAttribute("fotografie", fotografie);
        model.addAttribute("idFotografo", this.albumService.findById(idAlbum).getFotografo().getId());
        return "fotoByAlbum.html";
    }

    @RequestMapping(value="/admin/album/visualizza/{id}", method = RequestMethod.GET)
    public String visualizzaAlbumAdmin(@PathVariable("id") Long idAlbum, Model model){
        List<Foto> fotografie = this.fotografiaService.findByAlbum(this.albumService.findById(idAlbum));
        model.addAttribute("fotografie", fotografie);
        model.addAttribute("album", this.albumService.findById(idAlbum));
        model.addAttribute("idFotografo", this.albumService.findById(idAlbum).getFotografo().getId());
        return "fotoByAlbumADMIN.html";
    }

    @RequestMapping(value = "/admin/album/{id}/aggiungiFoto", method = RequestMethod.GET)
    public String aggiungiFoto(@PathVariable("id") Long idAlbum, Model model){
        model.addAttribute("idAlbum", idAlbum);
        return "fotografiaForm.html";
    }

    @RequestMapping(value = "/admin/album/{id}/cancella", method = RequestMethod.DELETE)
    public String cancellaAlbum(@PathVariable("id") Long idAlbum){
        Album album = this.albumService.findById(idAlbum);
        this.albumService.delete(album);
        return "redirect:/admin/viewFotografi";
    }

    @RequestMapping(value = "/admin/album/aggiungiUnAlbum/{id}/salvaAlbum", method = RequestMethod.POST)
    public String salvaAlbum(@ModelAttribute("album") Album album, @PathVariable("id") Long idFotografo){
        album.setFotografo(this.fotografoService.fotografoPerId(idFotografo));
        this.albumService.save(album);
        return "redirect:/admin/viewFotografi";
    }
}

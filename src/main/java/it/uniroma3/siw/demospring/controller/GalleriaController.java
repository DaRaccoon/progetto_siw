package it.uniroma3.siw.demospring.controller;

import it.uniroma3.siw.demospring.model.Foto;
import it.uniroma3.siw.demospring.services.AlbumService;
import it.uniroma3.siw.demospring.services.FotoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@AllArgsConstructor
public class GalleriaController {


    private FotoService fotografiaService;
    private AlbumService albumService;

    @RequestMapping(value ="/galleria", method = RequestMethod.GET )
    public String opzioniGalleria(){
        return "galleria.html";
    }

    @RequestMapping(value ="/galleria/tutte", method = RequestMethod.GET )
    public String visualizzaGalleria(Model model){
        List<Foto> galleria = this.fotografiaService.visualizzaTutte();
        model.addAttribute("galleria", galleria);
        return "fotografie.html";
    }

    @RequestMapping(value ="/galleria/tuttiAlbum", method = RequestMethod.GET)
    public String visualizzaAlbum(Model model){
        model.addAttribute("album", this.albumService.visualizzaTutti());
        return "tuttiGliAlbum.html";
    }
}



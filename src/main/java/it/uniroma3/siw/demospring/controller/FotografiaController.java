package it.uniroma3.siw.demospring.controller;
import it.uniroma3.siw.demospring.model.Foto;
import it.uniroma3.siw.demospring.model.Utente;
import it.uniroma3.siw.demospring.services.AlbumService;
import it.uniroma3.siw.demospring.services.FotoService;
import it.uniroma3.siw.demospring.services.FotografoService;
import it.uniroma3.siw.demospring.services.UtenteService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@AllArgsConstructor
public class FotografiaController {
    FotoService fotografiaService;


    final static String uploadDirectory = "C:/SIW Projects/Progetto_SIW/progetto-base/src/main/resources/static/uploads";
    private AlbumService albumService;
    private FotografoService fotografoService;
    private UtenteService utenteService;


    @RequestMapping(value ="/admin/album/{id}/aggiungiFoto/upload", method = RequestMethod.POST)
    public String aggiungiFotografia(@RequestParam("file") MultipartFile file, @PathVariable("id") Long idAlbum, @RequestParam("nome") String nome ) {
        try {
            Foto uploadedFile = fotografiaService.uploadFile(file, this.uploadDirectory, nome);
            fotografiaService.saveAndSetParam(uploadedFile, this.albumService.findById(idAlbum), this.albumService.findById(idAlbum).getFotografo());
        } catch (IOException e){
            e.printStackTrace();
        }
        return "redirect:/admin/viewFotografi";
    }

    @RequestMapping(value="/foto/{id}", method = RequestMethod.GET)
    public String visualizzaFoto(@PathVariable("id") Long id, Model model){
        model.addAttribute("foto", this.fotografiaService.findById(id));
        model.addAttribute("filePath", this.fotografiaService.findById(id).getFilePath());
        return "fotografia.html";
    }

    @RequestMapping(value="/admin/foto/{id}", method = RequestMethod.GET)
    public String visualizzaFotoADMIN(@PathVariable("id") Long idFoto, Model model){
        model.addAttribute("foto", this.fotografiaService.findById(idFoto));
        model.addAttribute("filePath", this.fotografiaService.findById(idFoto).getFilePath());
        model.addAttribute("idAlbum", this.fotografiaService.findById(idFoto).getAlbum().getId());
        return "fotografiaADMIN.html";
    }

    @RequestMapping(value="/admin/foto/{id}/elimina", method = RequestMethod.DELETE)
    public String eliminaFoto(@PathVariable("id") Long idFoto) throws IOException {
        Foto fotografia = this.fotografiaService.findById(idFoto);
        this.fotografiaService.delete(fotografia);
        return "redirect:/admin/viewFotografi";
    }

    @RequestMapping(value="/foto/{id}/compra", method = RequestMethod.GET)
    public String compraFoto(@PathVariable("id") Long idFoto, Model model){
        model.addAttribute("fotografia", this.fotografiaService.findById(idFoto));
        model.addAttribute("utente", new Utente());
        return "compraFotografiaForm.html";
    }

    @RequestMapping(value="/foto/{id}/compra/confermaModulo", method = RequestMethod.POST)
    public String confermaModulo(@PathVariable("id") Long idFoto, @ModelAttribute("utente") Utente utente){
        utente.setFotografia(this.fotografiaService.findById(idFoto));
        this.utenteService.setParametriESalva(utente);
        return "confermaModulo.html";
    }
}
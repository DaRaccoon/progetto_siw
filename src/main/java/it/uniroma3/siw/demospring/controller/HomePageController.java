package it.uniroma3.siw.demospring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller

public class HomePageController {

    @GetMapping("/")
    public String homePage(){
        return "homePage";
    }

    @GetMapping("/fotografi")
    public String mostraFotografi(Model model) {
        return "fotografiADMIN";
    }

    @GetMapping("/albums")
    public String mostraAlbum (Model model) {
        return "albums";
    }

    @GetMapping("/fotografie")
    public String mostraFotografie (Model model){
        return "fotografie";
    }
}

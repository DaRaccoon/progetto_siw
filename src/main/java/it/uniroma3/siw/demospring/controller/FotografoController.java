package it.uniroma3.siw.demospring.controller;

import it.uniroma3.siw.demospring.services.FotografoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@AllArgsConstructor
public class FotografoController {

    private FotografoService fotografoService;

    @RequestMapping(value = "/viewFotografi", method = RequestMethod.GET)
    public String viewFotografi(Model model) {
        model.addAttribute("fotografi", this.fotografoService.visualizzaTutti());
        return "fotografi.html";
    }

    @RequestMapping(value = "/fotografo/{id}", method = RequestMethod.GET)
    public String getFotografo(@PathVariable("id") Long id, Model model){
        model.addAttribute("fotografo", this.fotografoService.fotografoPerId(id));
        return "fotografo.html";
    }
}




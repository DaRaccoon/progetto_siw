package it.uniroma3.siw.demospring.controller;

import it.uniroma3.siw.demospring.model.Fotografo;
import it.uniroma3.siw.demospring.services.FotografoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
@AllArgsConstructor
public class AdminController {

    private FotografoService fotografoService;

    @RequestMapping
    public String adminPage(){
        return "admin.html";
    }

    @RequestMapping(value= "/newFotografo", method = RequestMethod.GET)
    public String newFotografo(Model model){
        model.addAttribute("fotografo", new Fotografo());
        return "fotografoForm.html";
    }

    @RequestMapping(value = "/", method= RequestMethod.POST)
    public String addFotografo(@ModelAttribute("fotografo") Fotografo fotografo){
        this.fotografoService.save(fotografo);
        return "admin.html";
    }

    @RequestMapping(value = "/viewFotografi", method = RequestMethod.GET)
    public String viewFotografi(Model model){
        model.addAttribute("fotografi", this.fotografoService.visualizzaTutti());
        return "fotografiADMIN.html";
    }

    @RequestMapping(value = "/fotografo/{id}", method = RequestMethod.GET)
    public String getFotografo(@PathVariable("id") Long id, Model model){
        model.addAttribute("fotografo", this.fotografoService.fotografoPerId(id));
        return "fotografoADMIN.html";
    }

    @RequestMapping(value="/fotografo/{id}/elimina", method = RequestMethod.GET)
    public String cancellaFotografo(@PathVariable("id") Long id){
        this.fotografoService.cancellaFotografo(id);
        return "redirect:/admin/viewFotografi";
    }
}

package it.uniroma3.siw.demospring.repository;

import it.uniroma3.siw.demospring.model.Album;
import it.uniroma3.siw.demospring.model.Foto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FotoRepository extends CrudRepository<Foto, Long> {
    List<Foto> findByAlbum(Album album);
    void delete(Foto fotografia);
}

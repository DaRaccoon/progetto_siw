package it.uniroma3.siw.demospring.repository;


import it.uniroma3.siw.demospring.model.Utente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtenteRepository extends JpaRepository<Utente, Long> {
}

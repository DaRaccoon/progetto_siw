package it.uniroma3.siw.demospring.repository;


import it.uniroma3.siw.demospring.model.Fotografo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface FotografoRepository extends CrudRepository<Fotografo, Long> {

}

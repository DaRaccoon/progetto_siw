package it.uniroma3.siw.demospring.repository;


import it.uniroma3.siw.demospring.model.Carrello;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarrelloRepository extends JpaRepository<Carrello, Long> {
}

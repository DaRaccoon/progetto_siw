package it.uniroma3.siw.demospring.services;

import it.uniroma3.siw.demospring.model.Foto;
import it.uniroma3.siw.demospring.model.Utente;
import it.uniroma3.siw.demospring.repository.UtenteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UtenteService {

    private UtenteRepository utenteRepository;

    public void setParametriESalva(Utente utente){
        this.utenteRepository.save(utente);
    }
}

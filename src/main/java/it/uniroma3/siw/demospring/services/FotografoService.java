package it.uniroma3.siw.demospring.services;

import it.uniroma3.siw.demospring.model.Fotografo;
import it.uniroma3.siw.demospring.repository.FotografoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FotografoService {

    @Autowired
    private FotografoRepository fotografoRepository;

    @Transactional
    public void save(Fotografo fotografo){
        this.fotografoRepository.save(fotografo);
    }

    @Transactional
    public List<Fotografo> visualizzaTutti(){

        return (List<Fotografo>) this.fotografoRepository.findAll();
    }

    @Transactional
    public Fotografo fotografoPerId(Long id){
        return this.fotografoRepository.findById(id).get();
    }

    @Transactional
    public void cancellaFotografo(Long id){
        this.fotografoRepository.delete(this.fotografoRepository.findById(id).get());
    }
}

package it.uniroma3.siw.demospring.services;

import it.uniroma3.siw.demospring.model.Album;
import it.uniroma3.siw.demospring.model.Fotografo;
import it.uniroma3.siw.demospring.repository.AlbumRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AlbumService {
    private AlbumRepository albumRepository;

    public void save(Album album){
        this.albumRepository.save(album);
    }

    public List<Album> visualizzaTutti(){
        return this.albumRepository.findAll();
    }

    public Album findById(Long id){
        return this.albumRepository.findById(id).get();
    }

    public List<Album> albumPerFotografo(Fotografo fotografo){
        return this.albumRepository.findByFotografo(fotografo);
    }

    public void delete(Album album) {
        this.albumRepository.delete(album);
    }
}

package it.uniroma3.siw.demospring.services;

import it.uniroma3.siw.demospring.model.Album;
import it.uniroma3.siw.demospring.model.Foto;
import it.uniroma3.siw.demospring.model.Fotografo;
import it.uniroma3.siw.demospring.repository.FotoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
@AllArgsConstructor
public class FotoService {
    private FotoRepository fotoRepository;

    String getFileExtension(String fileName){
        int dotIndex = fileName.lastIndexOf(".");
        if(dotIndex < 0) return null;

        return fileName.substring(dotIndex+1);
    }


    public Foto uploadFile(MultipartFile file, String uploadDirectory, String nome) throws IOException {
        String fileName = file.getOriginalFilename();
        Path path = Paths.get(uploadDirectory, fileName);
        Files.copy(file.getInputStream(), path);

        String extension = getFileExtension(fileName);
        String fileBaseName = nome;
        return new Foto(uploadDirectory, fileName, extension, fileBaseName);
    }

    public void save(Foto uploadedFile) {
        fotoRepository.save(uploadedFile);
    }

    public List<Foto> visualizzaTutte(){
        return (List<Foto>) fotoRepository.findAll();
    }

    public Foto findById(Long id){
        return this.fotoRepository.findById(id).get();
    }

    public void saveAndSetParam(Foto uploadedFile, Album album, Fotografo fotografo) {
        uploadedFile.setAlbum(album);
        uploadedFile.setFotografo(fotografo);
        this.fotoRepository.save(uploadedFile);
    }

    public List<Foto> findByAlbum(Album album) {
       return this.fotoRepository.findByAlbum(album);
    }

    public void delete(Foto fotografia) throws IOException {
        Files.deleteIfExists(Paths.get(fotografia.getCartellaFile(), fotografia.getFileName()));
        this.fotoRepository.delete(fotografia);
    }
}
package it.uniroma3.siw.demospring.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@Data
@EqualsAndHashCode
public class Utente {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column
    private String nome;

    @Column
    private String cognome;

    @Column
    private String email;

    @OneToOne
    private Foto fotografia;

    public Utente(){}

    public Utente(String nome, String cognome, Foto fotografia){
        this.nome = nome;
        this.cognome = cognome;
        this.fotografia = fotografia;
    }


}

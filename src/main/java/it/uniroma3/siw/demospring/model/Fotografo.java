package it.uniroma3.siw.demospring.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "fotografo")
@Data
@EqualsAndHashCode
public class Fotografo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column (nullable = false)
    private String nome;

    @Column
    private String cognome;

    @Column (nullable = false)
    private String email;

    @OneToMany(mappedBy = "fotografo", cascade = CascadeType.ALL)
    @MapKey(name = "id")
    private Map<Long, Album> album;

    @OneToMany(mappedBy = "fotografo", cascade = CascadeType.ALL)
    private Map<Long, Foto> fotografie;

    public Fotografo(String nome, String cognome, String email) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
    }

    public Fotografo(){

    }

    public void setAlbum(String nome){
        this.album = new HashMap<>();
        Album nuovo = new Album(nome, this);
        this.album.put(nuovo.getId(), nuovo);
    }

    public List<Album> tuttiAlbum() {
       return (List<Album>) this.album.values();
    }
}

package it.uniroma3.siw.demospring.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.nio.file.Path;
import java.nio.file.Paths;

@Entity
@Table(name="fotografie")
@Data
@EqualsAndHashCode
public class Foto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column (nullable = false)
    private String cartellaFile;

    @Column (nullable = false)
    private String fileName;

    @Column (nullable = false)
    private String fileExtension;

    @Column (nullable = false)
    private String fileBaseName;

    @ManyToOne
    private Album album;

    @ManyToOne
    private Fotografo fotografo;

    public Foto(){}

    public Foto(String cartellaFile, String fileName, String fileExtension,
                String fileBaseName){
        this.cartellaFile = cartellaFile;
        this.fileName = fileName;
        this.fileExtension = fileExtension;
        this.fileBaseName = fileBaseName;
    }

    public Path getFilePath() {
        if (fileName == null || cartellaFile == null) {
            return null;
        }

        return Paths.get(cartellaFile, fileName);
    }
}

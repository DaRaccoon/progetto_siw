package it.uniroma3.siw.demospring.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.*;

@Entity
@Table (name = "album")
@Data
@EqualsAndHashCode

public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column (nullable = false)
    private String titolo;

    @ManyToOne
    private Fotografo fotografo;

    @OneToMany(mappedBy = "album")
    protected List<Foto> fotografie;

    public Album(){
        this.fotografie = new ArrayList<>();
    }

    public Album (String titolo,  Fotografo fotografo){
        this.titolo = titolo;
        this.fotografo = fotografo;
        this.fotografie = new ArrayList<>();
    }
}